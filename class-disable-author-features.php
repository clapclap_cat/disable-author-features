<?php
/**
 * Plugin Name: Disable Author Features
 */
class Disable_Author_Features {
	public function init() {
		if ( ! current_user_can( 'administrator' ) ) {
			add_filter( 'show_admin_bar', '__return_false' );
			if ( is_admin() ) {
				add_action( 'admin_menu', array( __CLASS__, 'remove_admin_pages' ) );
				add_filter( 'admin_head', array( __CLASS__, 'remove_admin_bar_style_backend' ) );
				add_action( 'admin_head-profile.php', array( __CLASS__, 'remove_profile_sections' ) );
				add_filter( 'screen_options_show_screen', '__return_false' );
				add_filter( 'contextual_help_list', array( __CLASS__, 'contextual_help_list_remove' ) );
				add_action( 'pre_get_posts', array( __CLASS__, 'query_set_only_author' ) );
			}
		}
	}

	public static function remove_admin_pages() {
		remove_menu_page( 'index.php' );
		remove_menu_page( 'upload.php' );
		remove_menu_page( 'tools.php' );
	}

	public static function remove_admin_bar_style_backend() {
		echo '<style>#wpadminbar{display: none !important;} .wp-toolbar {padding-top: 0 !important;} .subsubsub .all, .subsubsub .publish { display: none !important; } .row-actions .edit, .inline-edit-status, .inline-edit-group { display: none !important; }</style>';
	}

	public static function remove_profile_sections() {
		echo '<style>.user-rich-editing-wrap,.user-admin-color-wrap,.user-syntax-highlighting-wrap,.user-admin-bar-front-wrap,#application-passwords-section,#fieldset-billing+h2,#fieldset-shipping,.wc-memberships,.user-profile-picture,#simple-local-avatar-section .ratings-row{display: none}</style>';
	}

	public static function contextual_help_list_remove() {
		global $current_screen;
		$current_screen->remove_help_tabs();
	}


	public static function query_set_only_author( WP_Query $wp_query ) {
		global $current_user;
		$wp_query->set( 'author', $current_user->ID );
	}
}

$disable_author_features = new Disable_Author_features();
add_action( 'init', array( $disable_author_features, 'init' ) );
